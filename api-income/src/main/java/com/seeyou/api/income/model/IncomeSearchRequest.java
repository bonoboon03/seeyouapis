package com.seeyou.api.income.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IncomeSearchRequest {
    private String incomeYear;
    private String incomeMonth;
    private String incomeCategory;
}
