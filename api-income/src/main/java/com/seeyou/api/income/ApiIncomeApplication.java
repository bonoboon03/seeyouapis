package com.seeyou.api.income;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiIncomeApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApiIncomeApplication.class, args);
    }
}
