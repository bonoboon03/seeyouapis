package com.seeyou.api.income.controller;

import com.seeyou.api.income.model.IncomeItem;
import com.seeyou.api.income.model.IncomeRequest;
import com.seeyou.api.income.model.IncomeResponse;
import com.seeyou.api.income.model.IncomeSearchRequest;
import com.seeyou.api.income.service.IncomeService;
import com.seeyou.common.response.model.CommonResult;
import com.seeyou.common.response.model.ListResult;
import com.seeyou.common.response.model.SingleResult;
import com.seeyou.common.response.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "지출 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/income")
public class IncomeController {
    private final IncomeService incomeService;

    @ApiOperation(value = "지출 등록")
    @PostMapping("/data")
    public CommonResult setIncome(@RequestBody @Valid IncomeRequest request) {
        incomeService.setIncome(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "지출 리스트")
    @GetMapping("/all/{page}")
    public ListResult<IncomeItem> getList(
            @PathVariable int page,
            @RequestParam(value = "searchYear", required = false) String searchYear,
            @RequestParam(value = "searchMonth", required = false) String searchMonth,
            @RequestParam(value = "searchName", required = false) String searchName
            ) {
        IncomeSearchRequest request = new IncomeSearchRequest();
        request.setIncomeYear(searchYear);
        request.setIncomeMonth(searchMonth);
        request.setIncomeCategory(searchName);
        return ResponseService.getListResult(incomeService.getList(page, request), true);
    }

    @ApiOperation(value = "지출 리스트 상세")
    @GetMapping("/income")
    public SingleResult<IncomeResponse> getIncome(@RequestParam("id") long id) {
        return ResponseService.getSingleResult(incomeService.getIncome(id));
    }

    @ApiOperation(value = "지출 수정")
    @PutMapping("/{id}")
    public CommonResult putIncome(@PathVariable long id, @RequestBody @Valid IncomeRequest request) {
        incomeService.putIncome(id, request);
        return ResponseService.getSuccessResult();
    }
}
