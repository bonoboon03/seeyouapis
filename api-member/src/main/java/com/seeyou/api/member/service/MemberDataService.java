package com.seeyou.api.member.service;

import com.seeyou.api.member.entity.Member;
import com.seeyou.api.member.model.*;
import com.seeyou.api.member.repository.MemberRepository;
import com.seeyou.common.enums.MemberGroup;
import com.seeyou.common.exception.CMissingDataException;
import com.seeyou.common.exception.CNotMatchPasswordException;
import com.seeyou.common.exception.CWrongPhoneNumberException;
import com.seeyou.common.function.CommonCheck;
import com.seeyou.common.response.model.ListResult;
import com.seeyou.common.response.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import javax.swing.border.Border;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberDataService {
    private final MemberRepository memberRepository;
    private final PasswordEncoder passwordEncoder;
    private final EntityManager entityManager;

    public void setFirstMember() {
        String username = "superyerin";
        String password = "12345678";
        String contact = "010-0000-0000";
        boolean isSuperYerin = isNewUsername(username);

        if (isSuperYerin) {
            MemberCreateRequest createRequest = new MemberCreateRequest();
            createRequest.setUsername(username);
            createRequest.setPassword(password);
            createRequest.setMemberName("점장");
            createRequest.setContact(contact);
            setMember(MemberGroup.ROLE_OWNER, createRequest);
        }
    }

    public void setMember(MemberGroup memberGroup, MemberCreateRequest createRequest) {
        if (!CommonCheck.checkUsername(createRequest.getUsername())) throw new CWrongPhoneNumberException(); // 유효한 아이디 형식이 아닙니다 던지기
        if (!isNewUsername(createRequest.getUsername())) throw new CWrongPhoneNumberException(); // 중복된 아이디가 존재합니다 던지기

        String password = "seeyoucoffee";
        createRequest.setPassword(passwordEncoder.encode(password));

        Member member = new Member.Builder(memberGroup, createRequest).build();
        memberRepository.save(member);
    }

    private boolean isNewUsername(String username) {
        long dupCount = memberRepository.countByUsername(username);
        return dupCount <= 0;
    }

    public ListResult<MemberItem> getMemberList(int page, MemberSearchRequest request) {
        PageRequest pageRequest = ListConvertService.getPageable(page, 10);
        Page<Member> originList = getMemberList(pageRequest, request);

        List<MemberItem> result = new LinkedList<>();

        originList.forEach(e -> result.add(new MemberItem.Builder(e).build()));

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    private Page<Member> getMemberList(Pageable pageable, MemberSearchRequest request) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Member> criteriaQuery = criteriaBuilder.createQuery(Member.class);

        Root<Member> root = criteriaQuery.from(Member.class);

        List<Predicate> predicates = new LinkedList<>();
        if (request.getMemberName() != null) predicates.add(criteriaBuilder.like(root.get("memberName"), "%" + request.getMemberName() + "%"));
        if (request.getMemberGroup() != null) predicates.add(criteriaBuilder.equal(root.get("memberGroup"), request.getMemberGroup()));
        if (request.getIsEnabled() != null) predicates.add(criteriaBuilder.equal(root.get("isEnabled"), request.getIsEnabled()));

        Predicate[] predArray = new Predicate[predicates.size()];
        predicates.toArray(predArray);
        criteriaQuery.where(predArray);
        criteriaQuery.orderBy(criteriaBuilder.desc(root.get("id")));

        TypedQuery<Member> query = entityManager.createQuery(criteriaQuery);

        int totalRows = query.getResultList().size();

        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());

        return new PageImpl<>(query.getResultList(), pageable, totalRows);
    }

    public MemberResponse getMemberLists(long id) {
        Member member = memberRepository.findById(id).orElseThrow();
        return new MemberResponse.Builder(member).build();
    }

    public void putMember(Member member, MemberUpdateRequest updateRequest) {
        member.putMember(updateRequest);
        memberRepository.save(member);
    }

    public void putIsEnabled(long id) {
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);
        member.putIsEnabled();
        memberRepository.save(member);
    }

    public void putPassword(Member member, PasswordChangeRequest changeRequest) {
        if(!passwordEncoder.matches(changeRequest.getCurrentPassword(), member.getPassword())) throw new CNotMatchPasswordException();

        if (!changeRequest.getChangePassword().equals(changeRequest.getChangePasswordRe())) throw new CNotMatchPasswordException();

        member.putPassword(passwordEncoder.encode(changeRequest.getChangePassword()));
        memberRepository.save(member);

    }

    public Member getMemberData(long id) {
        return memberRepository.findById(id).orElseThrow(CMissingDataException::new); // member를 id로 엿 바꿔주는 애
    }
}
