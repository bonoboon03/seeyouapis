package com.seeyou.api.member.controller;

import com.seeyou.api.member.entity.Member;
import com.seeyou.api.member.model.*;
import com.seeyou.api.member.service.MemberDataService;
import com.seeyou.api.member.service.ProfileService;
import com.seeyou.common.enums.MemberGroup;
import com.seeyou.common.response.model.CommonResult;
import com.seeyou.common.response.model.ListResult;
import com.seeyou.common.response.model.SingleResult;
import com.seeyou.common.response.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "직원 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberDataService memberDataService;
    private final ProfileService profileService;

    @ApiOperation(value = "직원 등록")
    @PostMapping("/data")
    public CommonResult setMember(@RequestBody @Valid MemberCreateRequest createRequest) {
        memberDataService.setMember(createRequest.getMemberGroup(), createRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "직원 리스트")
    @GetMapping("/member-list/{page}")
    public ListResult<MemberItem> getMemberList(
            @PathVariable int page,
            @RequestParam(value = "searchName", required = false) String searchName,
            @RequestParam(value = "searchGroup", required = false) MemberGroup searchGroup,
            @RequestParam(value = "searchEnable", required = false) String searchEnable
            ) {
        MemberSearchRequest request = new MemberSearchRequest();
        request.setMemberName(searchName);
        request.setMemberGroup(searchGroup);
        if (searchEnable != null) request.setIsEnabled(searchEnable.equals("O"));

        return ResponseService.getListResult(memberDataService.getMemberList(page, request), true);
    }

    @ApiOperation(value = "직원 리스트 상세")
    @GetMapping("/member-lists")
    public SingleResult<MemberResponse> getMemberLists(@RequestParam(value = "id") long id) {
        return ResponseService.getSingleResult(memberDataService.getMemberLists(id));
    }

    @ApiOperation(value = "직원 수정")
    @PutMapping("/{id}")
    public CommonResult putMember(@PathVariable long id, @RequestBody @Valid MemberUpdateRequest updateRequest) {
        Member member = memberDataService.getMemberData(id);
        memberDataService.putMember(member, updateRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "직원 퇴사")
    @PutMapping("/end/{id}")
    public CommonResult putIsEnabled(@PathVariable long id) {
        memberDataService.putIsEnabled(id);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "비밀번호 수정")
    @PutMapping("/password")
    public CommonResult putPassword(@RequestBody @Valid PasswordChangeRequest changeRequest) {
        Member member = profileService.getMemberData();
        memberDataService.putPassword(member, changeRequest);
        return ResponseService.getSuccessResult();
    }
}
