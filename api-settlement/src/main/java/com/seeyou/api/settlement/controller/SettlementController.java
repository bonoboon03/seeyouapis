package com.seeyou.api.settlement.controller;

import com.seeyou.api.settlement.model.SettlementItem;
import com.seeyou.api.settlement.model.SettlementResponse;
import com.seeyou.api.settlement.model.SettlementSearchRequest;
import com.seeyou.api.settlement.service.SettlementService;
import com.seeyou.common.response.model.CommonResult;
import com.seeyou.common.response.model.ListResult;
import com.seeyou.common.response.model.SingleResult;
import com.seeyou.common.response.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@Api(tags = "정산 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/settlement")
public class SettlementController {
    private final SettlementService settlementService;

    @ApiOperation(value = "정산 리스트")
    @GetMapping("/all/{page}")
    public ListResult<SettlementItem> getList(
            @PathVariable int page,
            @RequestParam(value = "searchYear", required = false) String searchYear,
            @RequestParam(value = "searchMonth", required = false) String searchMonth
            ) {
        SettlementSearchRequest request = new SettlementSearchRequest();
        request.setSettlementYear(searchYear);
        request.setSettlementMonth(searchMonth);
        return ResponseService.getListResult(settlementService.getList(page, request), true);
    }

    @ApiOperation(value = "정산액 보기")
    @GetMapping("/settlement")
    public SingleResult<SettlementResponse> getSettlement(@RequestParam("year") String year, @RequestParam("month") String month) {
        return ResponseService.getSingleResult(settlementService.getSettlement(year, month));
    }

    @ApiOperation(value = "정산 수정")
    @PutMapping("/update")
    public CommonResult putSettlement(@RequestParam("year") String year, @RequestParam("month") String month) {
        settlementService.putSettlement(year, month);
        return ResponseService.getSuccessResult();
    }
}
