package com.seeyou.api.settlement.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ProductOrderRequest {
    @ApiModelProperty(notes = "수량", required = true)
    @NotNull
    @Min(value = 0)
    private Integer quantity;
}
