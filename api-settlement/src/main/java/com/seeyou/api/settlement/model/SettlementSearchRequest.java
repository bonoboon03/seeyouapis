package com.seeyou.api.settlement.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SettlementSearchRequest {
    private String SettlementYear;
    private String SettlementMonth;
}
