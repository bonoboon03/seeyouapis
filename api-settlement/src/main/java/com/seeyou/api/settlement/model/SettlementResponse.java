package com.seeyou.api.settlement.model;

import com.seeyou.common.function.CommonFormat;
import com.seeyou.common.interfaces.CommonModelBuilder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class SettlementResponse {
    private BigDecimal price;

    private SettlementResponse(Builder builder) {
        this.price = builder.price;
    }

    public static class Builder implements CommonModelBuilder<SettlementResponse> {
        private final BigDecimal price;

        public Builder(double price) {
            this.price = CommonFormat.convertDoubleToDecimal(price);
        }

        @Override
        public SettlementResponse build() {
            return new SettlementResponse(this);
        }
    }
}
