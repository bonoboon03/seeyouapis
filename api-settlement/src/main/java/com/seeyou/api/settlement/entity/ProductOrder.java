package com.seeyou.api.settlement.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProductOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "productId", nullable = false)
    private Product product;

    @Column(nullable = false)
    private Integer quantity;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false)
    private Integer orderYear;

    @Column(nullable = false)
    private String orderMonth;

    @Column(nullable = false)
    private String orderDay;

    @Column(nullable = false)
    private LocalTime timeOrder;

    private LocalDateTime completeDateOrder;

    @Column(nullable = false)
    private Boolean isComplete;
}