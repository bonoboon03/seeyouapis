package com.seeyou.api.settlement.repository;

import com.seeyou.api.settlement.entity.Settlement;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SettlementRepository extends JpaRepository<Settlement, Long> {
    List<Settlement> findAllBySettlementYearAndSettlementMonth(String year, String month);
    Page<Settlement> findAllByIdGreaterThanEqualOrderByIdDesc(long id, Pageable pageable);
}
