package com.seeyou.api.settlement.service;

import com.seeyou.api.settlement.entity.Settlement;
import com.seeyou.api.settlement.model.SettlementItem;
import com.seeyou.api.settlement.model.SettlementResponse;
import com.seeyou.api.settlement.model.SettlementSearchRequest;
import com.seeyou.api.settlement.repository.ProductOrderRepository;
import com.seeyou.api.settlement.repository.SettlementRepository;
import com.seeyou.common.response.model.ListResult;
import com.seeyou.common.response.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SettlementService {
    private final SettlementRepository settlementRepository;
    private final EntityManager entityManager;

    public ListResult<SettlementItem> getList(int page, SettlementSearchRequest request) {
        PageRequest pageRequest = ListConvertService.getPageable(page, 10);
        Page<Settlement> originList = getList(pageRequest, request);

        List<SettlementItem> result = new LinkedList<>();

        originList.forEach(e -> result.add(new SettlementItem.Builder(e).build()));

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    private Page<Settlement> getList(Pageable pageable, SettlementSearchRequest request) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Settlement> criteriaQuery = criteriaBuilder.createQuery(Settlement.class);

        Root<Settlement> root = criteriaQuery.from(Settlement.class);

        List<Predicate> predicates = new LinkedList<>();
        if (request.getSettlementYear() != null) predicates.add(criteriaBuilder.equal(root.get("settlementYear"), request.getSettlementYear()));
        if (request.getSettlementMonth() != null) predicates.add(criteriaBuilder.like(root.get("settlementMonth"), request.getSettlementMonth()));

        Predicate[] predArray = new Predicate[predicates.size()];
        predicates.toArray(predArray);
        criteriaQuery.where(predArray);
        criteriaQuery.orderBy(criteriaBuilder.desc(root.get("id")));

        TypedQuery<Settlement> query = entityManager.createQuery(criteriaQuery);

        int totalRows = query.getResultList().size();

        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());

        return new PageImpl<>(query.getResultList(), pageable, totalRows);
    }

    public SettlementResponse getSettlement(String year, String month) {
        List<Settlement> originList = settlementRepository.findAllBySettlementYearAndSettlementMonth(year, month);
        double price = 0D;
        for (Settlement settlement : originList) {
            price = price + settlement.getPrice();
        }
        return new SettlementResponse.Builder(price).build();
    }

    public void putSettlement(String year, String month) {
        List<Settlement> originList = settlementRepository.findAllBySettlementYearAndSettlementMonth(year, month);
        for (Settlement settlement : originList) {
            settlement.putIsComplete();
            settlementRepository.save(settlement);
        }
    }
}