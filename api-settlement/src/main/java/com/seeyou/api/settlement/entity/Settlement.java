package com.seeyou.api.settlement.entity;

import com.seeyou.common.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Settlement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String settlementYear;

    @Column(nullable = false)
    private String settlementMonth;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false)
    private Boolean isComplete;

    public void putIsComplete() {
        this.isComplete = true;
    }

    private Settlement(Builder builder) {
        this.settlementYear = builder.settlementYear;
        this.settlementMonth = builder.settlementMonth;
        this.price = builder.price;
        this.isComplete = builder.isComplete;
    }

    public static class Builder implements CommonModelBuilder<Settlement> {
        private final String settlementYear;
        private final String settlementMonth;
        private final Double price;
        private final Boolean isComplete;

        public Builder(double price, String year, String month) {
            this.settlementYear = year;
            this.settlementMonth = month;
            this.price = price;
            this.isComplete = false;
        }

        @Override
        public Settlement build() {
            return new Settlement(this);
        }
    }
}