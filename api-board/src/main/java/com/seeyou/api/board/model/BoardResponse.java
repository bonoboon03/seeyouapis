package com.seeyou.api.board.model;

import com.seeyou.api.board.entity.Board;
import com.seeyou.common.function.CommonFormat;
import com.seeyou.common.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardResponse {
    @ApiModelProperty(notes = "제목")
    private String title;

    @ApiModelProperty(notes = "내용")
    private String content;

    @ApiModelProperty(notes = "작성일")
    private String dateWrite;

    private BoardResponse(Builder builder) {
        this.title = builder.title;
        this.content = builder.content;
        this.dateWrite = builder.dateWrite;
    }

    public static class Builder implements CommonModelBuilder<BoardResponse> {
        private final String title;
        private final String content;
        private final String dateWrite;

        public Builder(Board board) {
            this.title = board.getTitle();
            this.content = board.getContent();
            this.dateWrite = CommonFormat.convertLocalDateTimeToString(board.getDateWrite());
        }

        @Override
        public BoardResponse build() {
            return new BoardResponse(this);
        }
    }
}
