package com.seeyou.api.board.model;

import com.seeyou.api.board.entity.Board;
import com.seeyou.common.function.CommonFormat;
import com.seeyou.common.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardItem {
    @ApiModelProperty(notes = "시퀀스")
    private Long id;

    @ApiModelProperty(notes = "제목")
    private String title;

    @ApiModelProperty(notes = "작성일")
    private String dateWrite;

    private BoardItem(Builder builder) {
        this.id = builder.id;
        this.title = builder.title;
        this.dateWrite = builder.dateWrite;
    }

    public static class Builder implements CommonModelBuilder<BoardItem> {
        private final Long id;
        private final String title;
        private final String dateWrite;

        public Builder(Board board) {
            this.id = board.getId();
            this.title = board.getTitle();
            this.dateWrite = CommonFormat.convertLocalDateTimeToString(board.getDateWrite());
        }

        @Override
        public BoardItem build() {
            return new BoardItem(this);
        }
    }
}
