package com.seeyou.api.board.entity;

import com.seeyou.api.board.model.BoardRequest;
import com.seeyou.common.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Board {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 30)
    private String title;

    @Column(nullable = false, columnDefinition = "TEXT")
    private String content;

    @Column(nullable = false)
    private LocalDateTime dateWrite;

    public void putBoard(BoardRequest request) {
        this.title = request.getTitle();
        this.content = request.getContent();
    }

    private Board(Builder builder) {
        this.title = builder.title;
        this.content = builder.content;
        this.dateWrite = builder.dateWrite;
    }

    public static class Builder implements CommonModelBuilder<Board> {
        private final String title;
        private final String content;
        private final LocalDateTime dateWrite;

        public Builder(BoardRequest request) {
            this.title = request.getTitle();
            this.content = request.getContent();
            this.dateWrite = LocalDateTime.now();
        }

        @Override
        public Board build() {
            return new Board(this);
        }
    }
}
