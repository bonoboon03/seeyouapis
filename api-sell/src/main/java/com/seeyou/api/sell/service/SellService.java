package com.seeyou.api.sell.service;

import com.seeyou.api.sell.entity.Sell;
import com.seeyou.api.sell.entity.SellProduct;
import com.seeyou.api.sell.model.*;
import com.seeyou.api.sell.repository.SellProductRepository;
import com.seeyou.api.sell.repository.SellRepository;
import com.seeyou.common.function.CommonFile;
import com.seeyou.common.response.model.ListResult;
import com.seeyou.common.response.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SellService {
    private final SellRepository sellRepository;
    private final SellProductRepository sellProductRepository;
    private final EntityManager entityManager;

    public void setSells(MultipartFile csvFile) throws IOException {
        File file = CommonFile.multipartToFile(csvFile);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

        String line = "";
        int index = 0; // 줄 번호
        while ((line = bufferedReader.readLine()) != null) {
            if (index > 0) {

                String[] cols = line.split(",");

                SellProduct product = sellProductRepository.findById(Long.parseLong(cols[1])).orElseThrow();

                SellRequest sellRequest = new SellRequest();
                sellRequest.setBillNumber(Long.parseLong(cols[0]));
                sellRequest.setQuantity(Integer.parseInt(cols[2]));
                sellRequest.setPrice(Double.parseDouble(cols[3]));

                Sell sell = new Sell.Builder(product, sellRequest).build();
                sellRepository.save(sell); // 저장
            }
            index++; // index = index + 1;
        }
        bufferedReader.close();
    }

    public ListResult<SellItem> getList(int page, SellSearchRequest request) {
        PageRequest pageRequest = ListConvertService.getPageable(page, 10);
        Page<Sell> originList = getList(pageRequest, request);

        List<SellItem> result = new LinkedList<>();

        originList.forEach(e -> result.add(new SellItem.Builder(e).build()));

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    private Page<Sell> getList(Pageable pageable, SellSearchRequest request) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Sell> criteriaQuery = criteriaBuilder.createQuery(Sell.class);

        Root<Sell> root = criteriaQuery.from(Sell.class);

        List<Predicate> predicates = new LinkedList<>();
        if (request.getSellYear() != null) predicates.add(criteriaBuilder.equal(root.get("sellYear"), request.getSellYear()));
        if (request.getSellMonth() != null) predicates.add(criteriaBuilder.like(root.get("sellMonth"), request.getSellMonth()));
        if (request.getSellDay() != null) predicates.add(criteriaBuilder.like(root.get("sellDay"), request.getSellDay()));

        Predicate[] predArray = new Predicate[predicates.size()];
        predicates.toArray(predArray);
        criteriaQuery.where(predArray);
        criteriaQuery.orderBy(criteriaBuilder.desc(root.get("id")));

        TypedQuery<Sell> query = entityManager.createQuery(criteriaQuery);

        int totalRows = query.getResultList().size();

        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());

        return new PageImpl<>(query.getResultList(), pageable, totalRows);
    }

    // 판매 내역 환불 처리
    public void putIsCompleteUpdate(long billNumber) {
        List<Sell> originData = sellRepository.findAllByBillNumber(billNumber);

        for (Sell e : originData) {
            e.putIsCompleteUpdate(billNumber);
            sellRepository.save(e);
        }
    }

    public ListResult<SellItem> getBillNumberDetailList(long billNumber) {
        List<Sell> originData = sellRepository.findAllByBillNumber(billNumber);
        List<SellItem> result = new LinkedList<>();

        originData.forEach(e -> result.add(new SellItem.Builder(e).build()));

        return ListConvertService.settingResult(result);
    }
}