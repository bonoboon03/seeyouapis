package com.seeyou.api.product.model;

import com.seeyou.api.product.entity.Product;
import com.seeyou.common.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProductResponse {
    @ApiModelProperty(notes = "상품타입")
    private String productType;

    @ApiModelProperty(notes = "상품명")
    private String productName;

    @ApiModelProperty(notes = "단가")
    private Double unitPrice;

    @ApiModelProperty(notes = "사용여부")
    private String isEnabled;

    private ProductResponse(Builder builder) {
        this.productType = builder.productType;
        this.productName = builder.productName;
        this.unitPrice = builder.unitPrice;
        this.isEnabled = builder.isEnabled;
    }

    public static class Builder implements CommonModelBuilder<ProductResponse> {
        private final String productType;
        private final String productName;
        private final Double unitPrice;
        private final String isEnabled;

        public Builder(Product product) {
            this.productType = product.getProductType().getName();
            this.productName = product.getProductName();
            this.unitPrice = product.getUnitPrice();
            this.isEnabled = product.getIsEnabled() ? "O" : "X";
        }


        @Override
        public ProductResponse build() {
            return new ProductResponse(this);
        }
    }
}
