package com.seeyou.api.product.repository;


import com.seeyou.api.product.entity.Basket;
import com.seeyou.api.product.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BasketRepository extends JpaRepository<Basket, Long> {
    Optional<Basket> findByProduct(Product product);
}
