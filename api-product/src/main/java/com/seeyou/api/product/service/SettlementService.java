package com.seeyou.api.product.service;

import com.seeyou.api.product.entity.Settlement;
import com.seeyou.api.product.repository.SettlementRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
public class SettlementService {
    private final SettlementRepository settlementRepository;

    public void setSettlement(double price, String year, String month) {
        Settlement settlement = new Settlement.Builder(price, year, month).build();
        settlementRepository.save(settlement);
    }
}