package com.seeyou.api.product.service;

import com.seeyou.api.product.entity.Product;
import com.seeyou.api.product.entity.ProductOrder;
import com.seeyou.api.product.entity.Stock;
import com.seeyou.api.product.model.*;
import com.seeyou.api.product.repository.StockRepository;
import com.seeyou.common.exception.CMissingDataException;
import com.seeyou.common.response.model.ListResult;
import com.seeyou.common.response.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StockService {
    private final StockRepository stockRepository;
    private final EntityManager entityManager;

    public void setStock(Product product) {
        stockRepository.save(new Stock.Builder(product).build());
    }

    public ListResult<StockItem> getStockList(int page, StockSearchRequest request) {
        PageRequest pageRequest = ListConvertService.getPageable(page, 10);
        Page<Stock> originList = getStockList(pageRequest, request);

        List<StockItem> result = new LinkedList<>();

        originList.forEach(e -> result.add(new StockItem.Builder(e).build()));

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    private Page<Stock> getStockList(Pageable pageable, StockSearchRequest request) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Stock> criteriaQuery = criteriaBuilder.createQuery(Stock.class);

        Root<Stock> root = criteriaQuery.from(Stock.class);

        List<Predicate> predicates = new LinkedList<>();
        if (request.getName() != null) predicates.add(criteriaBuilder.like(root.get("product").get("productName"), "%" + request.getName() + "%"));

        Predicate[] predArray = new Predicate[predicates.size()];
        predicates.toArray(predArray);
        criteriaQuery.where(predArray);
        criteriaQuery.orderBy(criteriaBuilder.desc(root.get("id")));

        TypedQuery<Stock> query = entityManager.createQuery(criteriaQuery);

        int totalRows = query.getResultList().size();

        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());

        return new PageImpl<>(query.getResultList(), pageable, totalRows);
    }

    public StockResponse getStock(long id) {
        Stock stock = stockRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new StockResponse.Builder(stock).build();
    }

    public void putAutoUpdateStock(ProductOrder productOrder) {
        Stock stock = stockRepository.findByProduct(productOrder.getProduct()).orElseThrow(CMissingDataException::new);
        stock.putAutoUpdateStock(productOrder.getQuantity());
        stockRepository.save(stock);
    }

    public void putStock(long id, StockRequest request) {
        Stock stock = stockRepository.findById(id).orElseThrow(CMissingDataException::new);
        stock.putStock(request);
        stockRepository.save(stock);
    }

    public ListResult<StockItem> getStockLackList() {
        List<Stock> originData = stockRepository.findAll();
        List<StockItem> result = new LinkedList<>();

        for (Stock e : originData) {
            if (e.getStockQuantity() <= e.getMinQuantity()) {
                result.add(new StockItem.Builder(e).build());
            }
        }
        return ListConvertService.settingResult(result);
    }
}