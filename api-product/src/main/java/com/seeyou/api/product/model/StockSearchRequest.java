package com.seeyou.api.product.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StockSearchRequest {
    private String name;
}
