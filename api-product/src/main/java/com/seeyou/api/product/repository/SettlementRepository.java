package com.seeyou.api.product.repository;


import com.seeyou.api.product.entity.Settlement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SettlementRepository extends JpaRepository<Settlement, Long> {
}
