package com.seeyou.api.product.model;

import com.seeyou.common.enums.ProductType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ProductRequest {
    @ApiModelProperty(notes = "상품타입", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private ProductType productType;

    @ApiModelProperty(notes = "상품명", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String productName;

    @ApiModelProperty(notes = "단가", required = true)
    @NotNull
    @Min(value = 0)
    private Double unitPrice;
}
