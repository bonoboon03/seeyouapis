package com.seeyou.api.product.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class BasketRequest {
    @ApiModelProperty(notes = "수량")
    @NotNull
    @Min(value = 0)
    private Integer quantity;
}
