package com.seeyou.api.product.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class StockRequest {
    @ApiModelProperty(notes = "재고수량", required = true)
    @NotNull
    @Min(value = 0)
    private Integer stockQuantity;

    @ApiModelProperty(notes = "최소기준수량", required = true)
    @NotNull
    @Min(value = 0)
    private Integer minQuantity;
}
