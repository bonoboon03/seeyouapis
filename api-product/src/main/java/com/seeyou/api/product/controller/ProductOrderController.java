package com.seeyou.api.product.controller;

import com.seeyou.api.product.entity.ProductOrder;
import com.seeyou.api.product.model.ProductOrderCompleteItem;
import com.seeyou.api.product.model.ProductOrderItem;
import com.seeyou.api.product.model.ProductOrderResponse;
import com.seeyou.api.product.model.ProductOrderSearchRequest;
import com.seeyou.api.product.service.BasketService;
import com.seeyou.api.product.service.ProductOrderService;
import com.seeyou.api.product.service.SettlementService;
import com.seeyou.api.product.service.StockService;
import com.seeyou.common.response.model.CommonResult;
import com.seeyou.common.response.model.ListResult;
import com.seeyou.common.response.model.SingleResult;
import com.seeyou.common.response.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@Api(tags = "발주 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/product-order")
public class ProductOrderController {
    private final ProductOrderService productOrderService;
    private final StockService stockService;
    private final SettlementService settlementService;
    private final BasketService basketService;

    @ApiOperation(value = "발주 등록")
    @PostMapping("/data")
    public CommonResult setProductOrder() {
        basketService.delBasket();
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "발주 리스트")
    @GetMapping("/list/{page}")
    public ListResult<ProductOrderItem> getProductOrderList(
            @PathVariable int page,
            @RequestParam(value = "searchYear", required = false) String searchYear,
            @RequestParam(value = "searchMonth", required = false) String searchMonth,
            @RequestParam(value = "searchDay", required = false) String searchDay,
            @RequestParam(value = "searchComplete", required = false) String searchComplete
            ) {
        ProductOrderSearchRequest request = new ProductOrderSearchRequest();
        request.setOrderYear(searchYear);
        request.setOrderMonth(searchMonth);
        request.setOrderDay(searchDay);
        if (searchComplete != null) request.setIsComplete(searchComplete.equals("O"));
        return ResponseService.getListResult(productOrderService.getList(page, request), true);
    }

    @ApiOperation(value = "발주 리스트 상세")
    @GetMapping("/order")
    public SingleResult<ProductOrderResponse> getProductOrder(@RequestParam("id") long id) {
        return ResponseService.getSingleResult(productOrderService.getProductOrder(id));
    }

    @ApiOperation(value = "발주 상태 수정")
    @PutMapping("/complete/{id}")
    public CommonResult putProductOrderIsComplete(@PathVariable long id) {
        productOrderService.putProductOrder(id);
        ProductOrder productOrder = productOrderService.getProductData(id);
        stockService.putAutoUpdateStock(productOrder);
        settlementService.setSettlement(productOrder.getPrice(), productOrder.getOrderYear(), productOrder.getOrderMonth());

        return ResponseService.getSuccessResult();
    }
}