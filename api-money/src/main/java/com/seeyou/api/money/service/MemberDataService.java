package com.seeyou.api.money.service;

import com.seeyou.api.money.entity.Member;
import com.seeyou.api.money.model.*;
import com.seeyou.api.money.repository.MemberRepository;
import com.seeyou.common.enums.MemberGroup;
import com.seeyou.common.exception.CMissingDataException;
import com.seeyou.common.exception.CWrongPhoneNumberException;
import com.seeyou.common.function.CommonCheck;
import com.seeyou.common.response.model.ListResult;
import com.seeyou.common.response.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberDataService {
    private final MemberRepository memberRepository;

    public Member getMemberData(long id) {
        return memberRepository.findById(id).orElseThrow(CMissingDataException::new);
    }
}
