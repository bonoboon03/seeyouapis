package com.seeyou.api.money.model;

import com.seeyou.api.money.entity.MoneyHistory;
import com.seeyou.common.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MoneyHistoryResponse {
    @ApiModelProperty(notes = "직원명")
    private String member;

    @ApiModelProperty(notes = "년도")
    private String moneyYear;

    @ApiModelProperty(notes = "월")
    private String moneyMonth;

    @ApiModelProperty(notes = "세전금액")
    private Double beforeMoney;

    @ApiModelProperty(notes = "국민연금")
    private Double nationalPension;

    @ApiModelProperty(notes = "건강보험")
    private Double healthInsurance;

    @ApiModelProperty(notes = "장기요양보험")
    private Double longTermCareInsurance;

    @ApiModelProperty(notes = "고용보험")
    private Double employmentInsurance;

    @ApiModelProperty(notes = "산재보험")
    private Double industrialAccidentInsurance;

    @ApiModelProperty(notes = "소득세")
    private Double incomeTax;

    @ApiModelProperty(notes = "지방소득세")
    private Double localIncomeTax;

    @ApiModelProperty(notes = "총공제액")
    private Double totalDeductionAmount;

    @ApiModelProperty(notes = "급여")
    private Double money;

    private MoneyHistoryResponse(Builder builder) {
        this.member = builder.member;
        this.moneyYear = builder.moneyYear;
        this.moneyMonth = builder.moneyMonth;
        this.beforeMoney = builder.beforeMoney;
        this.nationalPension = builder.nationalPension;
        this.healthInsurance = builder.healthInsurance;
        this.longTermCareInsurance = builder.longTermCareInsurance;
        this.employmentInsurance = builder.employmentInsurance;
        this.industrialAccidentInsurance = builder.industrialAccidentInsurance;
        this.incomeTax = builder.incomeTax;
        this.localIncomeTax = builder.localIncomeTax;
        this.totalDeductionAmount = builder.totalDeductionAmount;
        this.money = builder.money;
    }

    public static class Builder implements CommonModelBuilder<MoneyHistoryResponse> {
        private final String member;
        private final String moneyYear;
        private final String moneyMonth;
        private final Double beforeMoney;
        private final Double nationalPension;
        private final Double healthInsurance;
        private final Double longTermCareInsurance;
        private final Double employmentInsurance;
        private final Double industrialAccidentInsurance;
        private final Double incomeTax;
        private final Double localIncomeTax;
        private final Double totalDeductionAmount;
        private final Double money;

        public Builder(MoneyHistory moneyHistory) {
            this.member = moneyHistory.getMember().getMemberName();
            this.moneyYear = moneyHistory.getMoneyYear();
            this.moneyMonth = moneyHistory.getMoneyMonth();
            this.beforeMoney = moneyHistory.getBeforeMoney();
            this.nationalPension = moneyHistory.getNationalPension();
            this.healthInsurance = moneyHistory.getHealthInsurance();
            this.longTermCareInsurance = moneyHistory.getLongTermCareInsurance();
            this.employmentInsurance = moneyHistory.getEmploymentInsurance();
            this.industrialAccidentInsurance = moneyHistory.getIndustrialAccidentInsurance();
            this.incomeTax = moneyHistory.getIncomeTax();
            this.localIncomeTax = moneyHistory.getLocalIncomeTax();
            this.totalDeductionAmount = nationalPension + healthInsurance + longTermCareInsurance + employmentInsurance + industrialAccidentInsurance + incomeTax + localIncomeTax;
            this.money = moneyHistory.getMoney();
        }
        @Override
        public MoneyHistoryResponse build() {
            return new MoneyHistoryResponse(this);
        }
    }
}