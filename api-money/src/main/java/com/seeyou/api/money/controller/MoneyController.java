package com.seeyou.api.money.controller;

import com.seeyou.api.money.entity.Member;
import com.seeyou.api.money.model.MoneyItem;
import com.seeyou.api.money.model.MoneyRequest;
import com.seeyou.api.money.model.MoneyResponse;
import com.seeyou.api.money.model.MoneySearchRequest;
import com.seeyou.api.money.service.MemberDataService;
import com.seeyou.api.money.service.MoneyService;
import com.seeyou.common.response.model.CommonResult;
import com.seeyou.common.response.model.ListResult;
import com.seeyou.common.response.model.SingleResult;
import com.seeyou.common.response.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "급여 내역")
@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/money")
public class MoneyController {
    private final MoneyService moneyService;
    private final MemberDataService memberDataService;

    @ApiOperation(value = "급여 등록")
    @PostMapping("/member-id/{memberId}")
    public CommonResult setMoney(@PathVariable long memberId, @RequestBody @Valid MoneyRequest moneyRequest) {
        Member member = memberDataService.getMemberData(memberId);
        moneyService.setMoney(member, moneyRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "급여 리스트")
    @GetMapping("/all/{page}")
    public ListResult<MoneyItem> getMoneyList(
            @PathVariable int page,
            @RequestParam(value = "searchName", required = false) String searchName
            ) {
        MoneySearchRequest request = new MoneySearchRequest();
        request.setName(searchName);
        return ResponseService.getListResult(moneyService.getMoneyList(page, request), true);
    }

    @ApiOperation(value = "급여 리스트 상세")
    @GetMapping("/alls")
    public SingleResult<MoneyResponse> getMoneyList(@RequestParam(value = "id") long id) {
        return ResponseService.getSingleResult(moneyService.getMoneyLists(id));
    }

    @ApiOperation(value = "급여 수정")
    @PutMapping("/{id}")
    public CommonResult putMoney(@PathVariable long id, @RequestBody @Valid MoneyRequest moneyRequest) {
        moneyService.putMoney(id, moneyRequest);
        return ResponseService.getSuccessResult();
    }
}