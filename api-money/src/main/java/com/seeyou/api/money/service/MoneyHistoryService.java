package com.seeyou.api.money.service;

import com.seeyou.api.money.entity.Attendance;
import com.seeyou.api.money.entity.Member;
import com.seeyou.api.money.entity.MoneyHistory;
import com.seeyou.api.money.model.MoneyHistoryItem;
import com.seeyou.api.money.model.MoneyHistoryRequest;
import com.seeyou.api.money.model.MoneyHistoryResponse;
import com.seeyou.api.money.model.MoneyHistorySearchRequest;
import com.seeyou.api.money.repository.AttendanceRepository;
import com.seeyou.api.money.repository.MoneyHistoryRepository;
import com.seeyou.common.enums.MoneyType;
import com.seeyou.common.exception.CMissingDataException;
import com.seeyou.common.response.model.ListResult;
import com.seeyou.common.response.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MoneyHistoryService {
    private final MoneyHistoryRepository moneyHistoryRepository;
    private final AttendanceRepository attendanceRepository;
    private final EntityManager entityManager;

    public void setMoneyHistory(Member member, MoneyHistoryRequest moneyHistoryRequest) {
        Optional<MoneyHistory> moneyHistory = moneyHistoryRepository.findByMemberAndMoneyYearAndMoneyMonth(member, moneyHistoryRequest.getMoneyYear(), moneyHistoryRequest.getMoneyMonth());

        if (moneyHistory.isPresent()) throw new CMissingDataException();

        List<Attendance> originList = attendanceRepository.findAllByAttendanceYearAndAttendanceMonthAndMember(LocalDate.now().getYear(), LocalDate.now().getMonthValue(), member);
        double totalMin = 0D;
        for (Attendance attendance : originList) {
            totalMin += Math.ceil(ChronoUnit.SECONDS.between(attendance.getTimeAttendance(), attendance.getTimeLeaveWork()) / 60.0);
        }

        double money = moneyHistoryRequest.getMoneyType().equals(MoneyType.WAGE) ? Math.floor(moneyHistoryRequest.getBeforeMoney() / 12) : (moneyHistoryRequest.getBeforeMoney() / 60) * totalMin;
        MoneyHistory addData = new MoneyHistory.Builder(member, money, moneyHistoryRequest).build();
        moneyHistoryRepository.save(addData);
    }

    public ListResult<MoneyHistoryItem> getList(int page, MoneyHistorySearchRequest request) {
        PageRequest pageRequest = ListConvertService.getPageable(page, 10);
        Page<MoneyHistory> originList = getList(pageRequest, request);

        List<MoneyHistoryItem> result = new LinkedList<>();

        originList.forEach(e -> result.add(new MoneyHistoryItem.Builder(e).build()));

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    private Page<MoneyHistory> getList(Pageable pageable, MoneyHistorySearchRequest request) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<MoneyHistory> criteriaQuery = criteriaBuilder.createQuery(MoneyHistory.class);

        Root<MoneyHistory> root = criteriaQuery.from(MoneyHistory.class);

        List<Predicate> predicates = new LinkedList<>();
        if (request.getMoneyYear() != null) predicates.add(criteriaBuilder.equal(root.get("moneyYear"), request.getMoneyYear()));
        if (request.getMoneyMonth() != null) predicates.add(criteriaBuilder.equal(root.get("moneyMonth"), request.getMoneyMonth()));
        if (request.getName() != null) predicates.add(criteriaBuilder.like(root.get("member").get("memberName"), "%" + request.getName() + "%"));

        Predicate[] predArray = new Predicate[predicates.size()];
        predicates.toArray(predArray);
        criteriaQuery.where(predArray);
        criteriaQuery.orderBy(criteriaBuilder.desc(root.get("id")));

        TypedQuery<MoneyHistory> query = entityManager.createQuery(criteriaQuery);

        int totalRows = query.getResultList().size();

        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());

        return new PageImpl<>(query.getResultList(), pageable, totalRows);
    }

    public MoneyHistoryResponse getMoneyHistory(long id) {
        MoneyHistory moneyHistory = moneyHistoryRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new MoneyHistoryResponse.Builder(moneyHistory).build();
    }

    public ListResult<MoneyHistoryItem> getMyMoneyHistories(Member member) {
        List<MoneyHistory> originList = moneyHistoryRepository.findByMember(member);
        List<MoneyHistoryItem> result = new LinkedList<>();
        for (MoneyHistory moneyHistory : originList) {
            result.add(new MoneyHistoryItem.Builder(moneyHistory).build());
        }
        return ListConvertService.settingResult(result);
    }

    public MoneyHistoryResponse getMyMoneyHistory(long id, Member member) {
        MoneyHistory moneyHistory = moneyHistoryRepository.findByIdAndMember(id, member).orElseThrow(CMissingDataException::new);
        return new MoneyHistoryResponse.Builder(moneyHistory).build();
    }
}