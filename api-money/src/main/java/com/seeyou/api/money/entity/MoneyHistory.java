package com.seeyou.api.money.entity;

import com.seeyou.api.money.model.MoneyHistoryRequest;
import com.seeyou.common.enums.MoneyType;
import com.seeyou.common.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MoneyHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @Column(nullable = false, length = 4)
    private String moneyYear;

    @Column(nullable = false, length = 2)
    private String moneyMonth;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private MoneyType moneyType;

    @Column(nullable = false)
    private Double beforeMoney;

    @Column(nullable = false)
    private Double nationalPension;

    @Column(nullable = false)
    private Double healthInsurance;

    @Column(nullable = false)
    private Double longTermCareInsurance;

    @Column(nullable = false)
    private Double employmentInsurance;

    @Column(nullable = false)
    private Double industrialAccidentInsurance;

    @Column(nullable = false)
    private Double incomeTax;

    @Column(nullable = false)
    private Double localIncomeTax;

    @Column(nullable = false)
    private Double money;

    private MoneyHistory(Builder builder) {
        this.member = builder.member;
        this.moneyYear = builder.moneyYear;
        this.moneyMonth = builder.moneyMonth;
        this.moneyType = builder.moneyType;
        this.beforeMoney = builder.beforeMoney;
        this.nationalPension = builder.nationalPension;
        this.healthInsurance = builder.healthInsurance;
        this.longTermCareInsurance = builder.longTermCareInsurance;
        this.employmentInsurance = builder.employmentInsurance;
        this.industrialAccidentInsurance = builder.industrialAccidentInsurance;
        this.incomeTax = builder.incomeTax;
        this.localIncomeTax = builder.localIncomeTax;
        this.money = builder.money;
    }

    public static class Builder implements CommonModelBuilder<MoneyHistory> {
        private final Member member;
        private final String moneyYear;
        private final String moneyMonth;
        private final MoneyType moneyType;
        private final Double beforeMoney;
        private final Double nationalPension;
        private final Double healthInsurance;
        private final Double longTermCareInsurance;
        private final Double employmentInsurance;
        private final Double industrialAccidentInsurance;
        private final Double incomeTax;
        private final Double localIncomeTax;
        private final Double money;

        public Builder(Member member, double money, MoneyHistoryRequest request) {
            this.member = member;
            this.moneyYear = request.getMoneyYear();
            this.moneyMonth = request.getMoneyMonth();
            this.moneyType = request.getMoneyType();
            this.beforeMoney = request.getBeforeMoney();
            this.nationalPension = request.getNationalPension();
            this.healthInsurance = request.getHealthInsurance();
            this.longTermCareInsurance = request.getLongTermCareInsurance();
            this.employmentInsurance = request.getEmploymentInsurance();
            this.industrialAccidentInsurance = request.getIndustrialAccidentInsurance();
            this.incomeTax = request.getIncomeTax();
            this.localIncomeTax = request.getLocalIncomeTax();
            this.money = money - (nationalPension + healthInsurance + longTermCareInsurance + employmentInsurance + industrialAccidentInsurance + incomeTax + localIncomeTax);
        }

        @Override
        public MoneyHistory build() {
            return new MoneyHistory(this);
        }
    }
}