package com.seeyou.api.attendance.model;

import com.seeyou.api.attendance.entity.Attendance;
import com.seeyou.common.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class AttendanceItem {
    @ApiModelProperty(notes = "시퀀스")
    private Long id;

    @ApiModelProperty(notes = "이름")
    private String name;

    @ApiModelProperty(notes = "날짜")
    private String dateAttendance;

    @ApiModelProperty(notes = "출근시간")
    private String timeAttendance;

    @ApiModelProperty(notes = "퇴근시간")
    private String timeLeaveWork;

    private AttendanceItem(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.dateAttendance = builder.dateAttendance;
        this.timeAttendance = builder.timeAttendance;
        this.timeLeaveWork = builder.timeLeaveWork;
    }

    public static class Builder implements CommonModelBuilder<AttendanceItem> {
        private final Long id;
        private final String name;
        private final String dateAttendance;
        private final String timeAttendance;
        private final String timeLeaveWork;

        public Builder(Attendance attendance) {
            this.id = attendance.getId();
            this.name = attendance.getMember().getMemberName();
            this.dateAttendance = attendance.getAttendanceYear() + "-" + attendance.getAttendanceMonth() + "-" + attendance.getAttendanceDay();
            this.timeAttendance = attendance.getTimeAttendance().getHour() + ":" + (attendance.getTimeAttendance().getMinute() < 10 ? "0" + attendance.getTimeAttendance().getMinute() : attendance.getTimeAttendance().getMinute());
            this.timeLeaveWork = attendance.getTimeLeaveWork() == null ? "-" : attendance.getTimeLeaveWork().getHour() + ":" + (attendance.getTimeLeaveWork().getMinute() < 10 ? "0" + attendance.getTimeLeaveWork().getMinute() : attendance.getTimeLeaveWork().getMinute());
        }

        @Override
        public AttendanceItem build() {
            return new AttendanceItem(this);
        }
    }
}
