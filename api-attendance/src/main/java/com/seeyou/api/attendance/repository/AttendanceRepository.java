package com.seeyou.api.attendance.repository;

import com.seeyou.api.attendance.entity.Attendance;
import com.seeyou.api.attendance.entity.Member;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AttendanceRepository extends JpaRepository<Attendance, Long> {
    Page<Attendance> findAllByIdGreaterThanEqualAndMemberOrderByIdDesc(long id, Member member, Pageable pageable);
    Optional<Attendance> findByAttendanceYearAndAttendanceMonthAndAttendanceDayAndMember(int year, int month, int day, Member member);
}