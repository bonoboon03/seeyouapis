package com.seeyou.api.attendance.entity;

import com.seeyou.common.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Attendance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @Column(nullable = false)
    private Integer attendanceYear;

    @Column(nullable = false)
    private Integer attendanceMonth;

    @Column(nullable = false)
    private Integer attendanceDay;

    @Column(nullable = false)
    private LocalTime timeAttendance;

    private LocalTime timeLeaveWork;

    public void leaveWork() {
        this.timeLeaveWork = LocalTime.now();
    }

    private Attendance(Builder builder) {
        this.member = builder.member;
        this.attendanceYear = builder.attendanceYear;
        this.attendanceMonth = builder.attendanceMonth;
        this.attendanceDay = builder.attendanceDay;
        this.timeAttendance = builder.timeAttendance;
    }

    public static class Builder implements CommonModelBuilder<Attendance> {
        private final Member member;
        private final Integer attendanceYear;
        private final Integer attendanceMonth;
        private final Integer attendanceDay;
        private final LocalTime timeAttendance;

        public Builder(Member member) {
            this.member = member;
            this.attendanceYear = LocalDate.now().getYear();
            this.attendanceMonth = LocalDate.now().getMonthValue();
            this.attendanceDay = LocalDate.now().getDayOfMonth();
            this.timeAttendance = LocalTime.now();
        }

        @Override
        public Attendance build() {
            return new Attendance(this);
        }
    }
}