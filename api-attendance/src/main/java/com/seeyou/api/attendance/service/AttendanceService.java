package com.seeyou.api.attendance.service;

import com.seeyou.api.attendance.entity.Attendance;
import com.seeyou.api.attendance.entity.Member;
import com.seeyou.api.attendance.model.AttendanceItem;
import com.seeyou.api.attendance.model.AttendanceSearchRequest;
import com.seeyou.api.attendance.repository.AttendanceRepository;
import com.seeyou.common.exception.CMissingDataException;
import com.seeyou.common.response.model.ListResult;
import com.seeyou.common.response.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AttendanceService {
    private final AttendanceRepository attendanceRepository;
    private final EntityManager entityManager;

    public void setAttendance(Member member) {
        Optional<Attendance> attendance = attendanceRepository.findByAttendanceYearAndAttendanceMonthAndAttendanceDayAndMember(LocalDate.now().getYear(), LocalDate.now().getMonthValue(), LocalDate.now().getDayOfMonth(), member);
        if (attendance.isEmpty()) {
            Attendance addData = new Attendance.Builder(member).build();
            attendanceRepository.save(addData);
        }
    }

    public ListResult<AttendanceItem> getList(int page, AttendanceSearchRequest request) {
        PageRequest pageRequest = ListConvertService.getPageable(page, 10);
        Page<Attendance> originList = getList(pageRequest, request);

        List<AttendanceItem> result = new LinkedList<>();

        originList.forEach(e -> result.add(new AttendanceItem.Builder(e).build()));

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    private Page<Attendance> getList(Pageable pageable, AttendanceSearchRequest request) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Attendance> criteriaQuery = criteriaBuilder.createQuery(Attendance.class);

        Root<Attendance> root = criteriaQuery.from(Attendance.class);

        List<Predicate> predicates = new LinkedList<>();
        if (request.getAttendanceYear() != null) predicates.add(criteriaBuilder.equal(root.get("attendanceYear"), request.getAttendanceYear()));
        if (request.getAttendanceMonth() != null) predicates.add(criteriaBuilder.equal(root.get("attendanceMonth"), request.getAttendanceMonth()));
        if (request.getAttendanceDay() != null) predicates.add(criteriaBuilder.equal(root.get("attendanceDay"), request.getAttendanceDay()));
        if (request.getName() != null) predicates.add(criteriaBuilder.like(root.get("member").get("memberName"), "%" + request.getName() + "%"));

        Predicate[] predArray = new Predicate[predicates.size()];
        predicates.toArray(predArray);
        criteriaQuery.where(predArray);
        criteriaQuery.orderBy(criteriaBuilder.desc(root.get("id")));

        TypedQuery<Attendance> query = entityManager.createQuery(criteriaQuery);

        int totalRows = query.getResultList().size();

        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());

        return new PageImpl<>(query.getResultList(), pageable, totalRows);
    }

    public ListResult<AttendanceItem> getMyList(int page, Member member) {
        Page<Attendance> originList = attendanceRepository.findAllByIdGreaterThanEqualAndMemberOrderByIdDesc(1, member, ListConvertService.getPageable(page));
        List<AttendanceItem> result = new LinkedList<>();
        for (Attendance attendance : originList) {
            result.add(new AttendanceItem.Builder(attendance).build());
        }

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    public void putLeaveWork(Member member) {
        Optional<Attendance> attendance = attendanceRepository.findByAttendanceYearAndAttendanceMonthAndAttendanceDayAndMember(LocalDate.now().getYear(), LocalDate.now().getMonthValue(), LocalDate.now().getDayOfMonth(), member);
        if (attendance.isEmpty()) throw new CMissingDataException();
        Attendance originData = attendance.get();
        originData.leaveWork();
        attendanceRepository.save(originData);
    }
}
