package com.seeyou.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MemberGroup {
    ROLE_OWNER("점장")
    , ROLE_MANAGER("매니저")
    , ROLE_EMPLOYEE("사원")
    , ROLE_PART_TIME("알바");

    private final String rankName;
}
