package com.seeyou.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SellProductType {

    COFFEE("커피"),

    NON_COFFEE("논커피"),

    ADE("에이드"),

    TEA("티"),

    DESSERT("디저트");

    private final String name;
}
